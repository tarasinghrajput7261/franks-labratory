// the variable that will define the default state of the animations
let playerState = 'idle';
// constant that contain the html element which contain the id 'animations'
const dropdown = document.getElementById('animations');
// It is a eventlistener  that hears the event 'change' for the dropdown element and runs the function for every element
dropdown.addEventListener('change', (e) => {
    // playerstate is assigned with the value attribute of the element currently in the forEach loop that is the targeted element
    playerState = e.target.value;
})
// a constant 'canvas' which holds the canvas element form the html file
const canvas = document.getElementById('canvas1');
// A constant 'ctx' which holds the '2d' context of the canvas
const ctx = canvas.getContext('2d');

// the constant 'CANVAS_WIDTH' and 'canvas.width' is assigned with the value of 600
const CANVAS_WIDTH = canvas.width = 600;
// the constant 'CANVAS_HEIGHT' and 'canvas.height' is assigned with the value of 600
const CANVAS_HEIGHT = canvas.height = 600;

// The 'playerImage' constant is assigned by a new object named Image
const playerImage = new Image();
// this line sets the source of the object 'playerImage' for the image
playerImage.src = 'shadow_dog.png';
// this constant 'spriteWidth' is assigned with the value of 575
const spriteWidth = 575;
// this constant 'spriteHeight' is assigned with the value of 523
const spriteHeight = 523;
// 'gameFrame' will be usefull to decide the frames for the animation
let gameFrame = 0;
// 'staggeredFrames' will be usefull for the frames that I want to decide the animation speed
let staggeredFrames = 5;
// A constant empty array 
const spriteAnimations = [];
// A constant array of objects to store the no.of frames for every animations state
const animationStates = [
    {
        name: 'idle',
        frames: 7
    },
    {
        name: 'jump',
        frames: 7
    },
    {
        name: 'fall',
        frames: 7
    },
    {
        name: 'run',
        frames: 9
    },
    {
        name: 'dizzy',
        frames: 11
    },
    {
        name: 'sit',
        frames: 5
    },
    {
        name: 'roll',
        frames: 7
    },
    {
        name: 'bite',
        frames: 7
    },
    {
        name: 'ko',
        frames: 12
    },
    {
        name: 'getHit',
        frames: 4
    },
];
// this forEach loop runs a function for every object in the 'animationStates' array, 'state' is the element at which the loop currently is
// and 'index' is the index of the element at which that current element is present in the 'animationStates' array
animationStates.forEach((state, index) => {
    // this defines a object 'frames' with an empty array 'loc' 
    let frames = {
        loc: [],
    }
// for loop to loop 'state.frames' for every 'state'
    for(let j = 0; j < state.frames; j++) {
        // so that the 1st time the 'j' will be 0 and 'positionX' will become 0 to start the X position to be 0
        let positionX = j * spriteWidth;
        // so that the 1st time the 'index' will be 0 and 'positonY' will become 0 to start the Y position to be 0 and since the 'index' targets the current element of the 'animationStates' array it will increase as it jumps to the next element in the 'animationStates' array
        let positionY = index * spriteHeight;
        // it pushes a object made of the above variables 'positionX' and 'positionY' to the 'loc' array of the frames object
        frames.loc.push({x: positionX, y: positionY});
    } //for loop ends here
    // now here we assign the empty 'spriteAnimations' array with the new object of key named 'state.name' and value the 'loc' object
    spriteAnimations[state.name] = frames;
})
console.log(spriteAnimations);
// a function named 'animate()'
function animate() {
    // the 'clearRect()' method help to erase the area in under which the parameters are x  position, y position and the height and width of the rect
    ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
    //  it assigns a new variable named 'position' so that to eliminate the problem of putting the no. of frames by hardcoding the value
    let position = Math.floor(gameFrame/staggeredFrames) % spriteAnimations[playerState].loc.length;
    // to store the value of each frame's x position distance from (0,0)
    let frameX = spriteWidth * position;
    // to store the value of each frame's y position distance from (0,0)
    let frameY = spriteAnimations[playerState].loc[position].y;
    // it draws a image of named 'playerImage', from 'frameX' and 'frameY' distance of the 'playerImage' of height of 'spriteHeight' and width of 'spriteWidth' and draw this image from (0,0) position of the canvas of 'spriteWidth' of width and 'spriteHeight' of height
    ctx.drawImage(playerImage, frameX, frameY, spriteWidth, spriteHeight, 0, 0, spriteWidth, spriteHeight);
    // this line increases the gameFrame every time the animate() method runs, it is used to track the no. of times the animate() method is called
    gameFrame++;
    // this method calls the animation method to create a loop
    requestAnimationFrame(animate);
};
// this is called here to start the loop
animate();